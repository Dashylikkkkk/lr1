require('dotenv').config();
const fs = require('fs');

const COURSE_DOL_RUB = Number(process.env.COURSE_DOL_RUB) || 60; // курс доллара к рублю 
const CONVERT_RUB_TO_DOL = Boolean(process.env.CONVERT_RUB_TO_DOL) || true; // переводить рубль в доллары или наоборот
const SUM_TO_CONVERT = Number(process.env.SUM_TO_CONVERT) || 240; // сумма для конвертации

let converted = 0;
if (CONVERT_RUB_TO_DOL){
    converted = SUM_TO_CONVERT / COURSE_DOL_RUB;
}else{
    converted = SUM_TO_CONVERT * COURSE_DOL_RUB
}
const add_text = CONVERT_RUB_TO_DOL ? 'долларов' : 'рублей';
const text = `Переведенная сумма составила ${converted} ${add_text}`
console.log(text);

fs.writeFileSync('output/result.txt',text)